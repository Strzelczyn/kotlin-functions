fun main(args: Array<String>) {
    var firstValue: Int = 3
    var secondValue: Int = 2
    var myResult = multiplying(firstValue, secondValue)
    show(myResult)
}

fun multiplying(left: Int, right: Int): Int {
    return left * right
}

fun show(myValue: Int) {
    println("Value is $myValue")
}